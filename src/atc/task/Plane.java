package atc.task;

import java.awt.Color;
import java.awt.Font;

import javax.swing.SwingConstants;

import actr.task.TaskLabel;

public class Plane extends TaskLabel {

	public Plane(String callSign, int x, int y) {
		super(callSign, x, y, 60, 30);
		setHorizontalAlignment(SwingConstants.CENTER);
		setVerticalAlignment(SwingConstants.TOP);
		setFont(new Font("Courier", Font.BOLD, 18));
		setForeground(Color.GREEN);
	}
}
