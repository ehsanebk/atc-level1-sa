package atc.task;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import actr.task.Task;
import actr.task.TaskComponent;

public class Flight extends JPanel implements TaskComponent {
	private static final int HEADING_LENGTH = 30;
	private static final int DATABLOCK_TEXT_DX = 75;
	private static final int DATABLOCK_TEXT_DY = 40;
	private static final int DATABLOCK_LINE_DX = 25;
	private static final int DATABLOCK_LINE_DY = 15;

	private Task task;
	private Route route;
	private String callSign;
	private int assignedAltitude;
	private int currentAltitude;
	private double traveled;
	private PointHeading point;
	private Text dataBlock1;
	private Altitude dataBlockAltitude;
	private Speed dataBlockSpeed;
	private int currentSpeed;
	private Distance distanceToIntersection;
	

	public Flight(Task task, Route route, String callSign) {
		super();
		this.task = task;
		this.route = route;
		this.callSign = callSign;
		Random random = new Random();
		assignedAltitude = 330;
		currentAltitude = 100 + random.nextInt(599);
		currentSpeed = 200 + random.nextInt(299);
		traveled = 0;
		point = route.getPointHeading(traveled);
		setBounds(point.getIntX() , point.getIntY(), 1, 1);

		dataBlock1 = new Text(callSign, 0, 0, DATABLOCK_TEXT_DX, 10);
		dataBlockAltitude = new Altitude(this,"", 0, 0, DATABLOCK_TEXT_DX, 10);
		dataBlockSpeed = new Speed(this,"", 0, 0, DATABLOCK_TEXT_DX, 10);
		
		distanceToIntersection = new Distance(this,0,0 , 10, 10);
		
		
		task.add(dataBlock1, 0);
		task.add(dataBlockAltitude, 0);
		task.add(dataBlockSpeed, 0);
		task.add(distanceToIntersection,0);
		fixDataBlock();
	}
	
	public Flight(Task task,PointHeading p, String callSign) {
		super();
		this.task = task;
		this.callSign = callSign;
		this.point = p;
		Random random = new Random();
		assignedAltitude = 330;
		currentAltitude = 100 + random.nextInt(599);
		currentSpeed = 200 + random.nextInt(299);
		setBounds(point.getIntX() , point.getIntY(), 10, 10);
		//setBorder(BorderFactory.createLineBorder(Color.white));

		dataBlock1 = new Text(callSign, 0, 0, DATABLOCK_TEXT_DX, 10);
		dataBlockAltitude = new Altitude(this,"", 0, 0, DATABLOCK_TEXT_DX, 10);
		dataBlockSpeed = new Speed(this,"", 0, 0, DATABLOCK_TEXT_DX, 10);
		
		
		task.add(dataBlock1, 0);
		task.add(dataBlockAltitude, 0);
		task.add(dataBlockSpeed, 0);
		
		fixDataBlock();
	}

	private void fixDataBlock() {
		int spacing = 10;
		dataBlockAltitude.setText(assignedAltitude + " " + (assignedAltitude == currentAltitude ? "-"
				: assignedAltitude > currentAltitude ? "\u2191 " + currentAltitude : "\u2193 " + currentAltitude));
		dataBlockSpeed.setText("123 D " + currentSpeed );
		dataBlock1.setLocation(point.getIntX() - DATABLOCK_TEXT_DX, point.getIntY() - DATABLOCK_TEXT_DY);
		dataBlockAltitude.setLocation(point.getIntX() - DATABLOCK_TEXT_DX, point.getIntY() - DATABLOCK_TEXT_DY + spacing);
		dataBlockSpeed.setLocation(point.getIntX() - DATABLOCK_TEXT_DX, point.getIntY() - DATABLOCK_TEXT_DY + 2 * spacing);

	}

	public void moveTo(double traveled) {
		this.traveled = traveled;
		point = route.getPointHeading(traveled);
		setLocation(point.getIntX(), point.getIntY());
		fixDataBlock();
	}

	public void moveBy(double distance) {
		traveled += distance;
		if (traveled < 0)
			traveled += route.getLength();
		if (traveled > route.getLength())
			traveled -= route.getLength();
		moveTo(traveled);
	}

	public String getCallSign() {
		return callSign;
	}

	public int getAssignedAltitude() {
		return assignedAltitude;
	}

	public int getCurrentAltitude() {
		return currentAltitude;
	}
	
	public int getCurrentSpeed() {
		return currentSpeed;
	}

	public double getDistanceToIntesection() {
		
		return route.getLength()-traveled;
	}

	public void setAssignedAltitude(int altitude) {
		assignedAltitude = altitude;
		fixDataBlock();
	}

	public void setCurrentAltitude(int altitude) {
		currentAltitude = altitude;
		fixDataBlock();
	}

	public String getKind() {
		return "plane";
	}

	public String getValue() {
		return callSign;
	}

	@Override
	public void paintComponent(Graphics g) {
		int size = 3;
		int max = Math.max(Math.max(DATABLOCK_LINE_DX, DATABLOCK_LINE_DY), HEADING_LENGTH);
		Graphics2D g2 = (Graphics2D) g.create();
		g2.setClip(-max, -max, 2 * max, 2 * max);
		g2.setColor(Color.gray);
		g2.drawLine(-DATABLOCK_LINE_DX, -DATABLOCK_LINE_DY, -size - 2, -size - 1);
		g2.setColor(Color.green);
		Point start = new Point(0, 0).translateByHeading(HEADING_LENGTH / 3, point.getHeading() + Math.PI);
		Point end = new Point(0, 0).translateByHeading(HEADING_LENGTH, point.getHeading());	
		g2.drawLine(start.getIntX(), start.getIntY(), end.getIntX(), end.getIntY());
		g2.drawRect(-size, -size, 2 * size, 2 * size);
		g2.dispose();
	}
}
