package atc.task;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Vector;

import actr.model.Symbol;
import actr.task.Result;
import actr.task.Statistics;
import actr.task.Task;
import actr.task.Utilities;
import sun.text.normalizer.Trie.DataManipulate;

public class ATC_old extends Task {

	double[] humanCounts = {};

	private final int NUMBER_OF_PLANES = 4;
	private final int NUMBER_OF_TRIALS = 50;
	private final int NUMBER_OF_SAMPLES = NUMBER_OF_TRIALS * NUMBER_OF_PLANES;
	int sampleIndex = 0;
	int trialNumber =0;
	double lastTime = 0;
	double responseTime = 0;
	double responseTimes[] = new double[NUMBER_OF_SAMPLES];
	int recalledObjects = 0;
	int forgottenObjects =0;

	// responses is a two dimentional vector which the first element is the
	// location error and the second is the response time
	double[] responsesLocationError = new double[NUMBER_OF_SAMPLES];
	double[] meanAccuracyAtEachTrail = new double[NUMBER_OF_TRIALS];
	
	int flightIndex = 0;

	private List<PointHeading> flightLocations;
	private List<Flight> flights;

	public ATC_old() {
		super();
		setBackground(Color.black);

	}

	@Override
	public void start() {
		sampleIndex = 0;
		repaint();
		addUpdate(0);
	}

	@Override
	public void update(double time) {
		getModel().getVision().clearVisual();// clearing the vision module
		if (trialNumber < NUMBER_OF_TRIALS) {
			//getModel().setParameter(":rt", "1");
			
			//System.out.println("trail number==> " + trialNumber);
			Random random = new Random();
			flightLocations = new Vector<PointHeading>();
			flights = new Vector<Flight>();
			PointHeading p;
			// defining the random coordinates of the planes based on the
			// Rantanen experiments where the display size was 1024*728
			for (int i = 0; i < NUMBER_OF_PLANES; i++) {
				String name = "AA" + (400 + sampleIndex + i);
				if (i % 4 == 0)
					p = new PointHeading(random.nextInt(1024 / 2), random.nextInt(768 / 2),
							random.nextDouble() * 2 * Math.PI);
				else if (i % 4 == 1)
					p = new PointHeading(random.nextInt(1024 / 2) + 1024 / 2, random.nextInt(768 / 2),
							random.nextDouble() * 2 * Math.PI);
				else if (i % 4 == 2)
					p = new PointHeading(random.nextInt(1024 / 2), random.nextInt(768 / 2) + 768 / 2,
							random.nextDouble() * 2 * Math.PI);
				else
					p = new PointHeading(random.nextInt(1024 / 2) + 1024 / 2, random.nextInt(768 / 2) + 768 / 2,
							random.nextDouble() * 2 * Math.PI);

				flightLocations.add(p);
				Flight flight = new Flight(this, p, name);
				flights.add(flight);

				add(flight, 0);
			}

			flightIndex = 0;
			double timeDelta = 0.7 * NUMBER_OF_PLANES;
			getModel().addEvent(new actr.model.Event(getModel().getTime() + timeDelta + 1.00, "task", "update") {
				public void action() {
					SayingCallSigns();
				}

			});

			repaint();
			processDisplay();
		} else {
			getModel().stop();
		}

	}

	
	private void SayingCallSigns() {

		removeAll();
		repaint();
		processDisplay();
		addAural(0.10, "CallSign", "sound", flights.get(flightIndex).getValue());
		lastTime = getModel().getTime() + 0.10;
	}

	@Override
	public void eval(Iterator<String> it) {
		it.next(); //skipping the first parentheses
		String function = it.next();
		if (function.equals("add-to-racalled-objects")){
			recalledObjects++ ;
		}
		
		if (function.equals("add-to-forgotten-objects")){
			forgottenObjects++ ;
		}
		
		if (function.equals("delete-rehearse-goal")){
			//getModel().getDeclarative().get(Symbol.get("rehearse"));
		}

		if (function.equals("end-rehearse")){
			//getModel().getDeclarative().get(Symbol.get("rehearse"));
		}

	}

	@Override
	public void clickMouse() {

		Random random = new Random();
		// applying the normal distribution error on the locations selected by
		// the model
		double mouseX = random.nextGaussian() * NUMBER_OF_PLANES + getMouseX();
		double mouseY = random.nextGaussian() * NUMBER_OF_PLANES + getMouseY();

		// Implementing what happens upon mouse click by the model
		// Point2D mouse = new Point2D.Double(getMouseX(),getMouseY());
		Point2D mouse = new Point2D.Double(mouseX, mouseY);
		//System.out.println(flightLocations.get(flightIndex).distance(mouse));
		// for some reason there is a 7.07 error when even there is no error
		// distribution
		responsesLocationError[sampleIndex] = flightLocations.get(flightIndex).distance(mouse);
		responseTimes[sampleIndex] = getModel().getTime() - lastTime;
		sampleIndex++;
		
		

		if (flightIndex < NUMBER_OF_PLANES - 1) {
			flightIndex++;
			getModel().addEvent(new actr.model.Event(getModel().getTime(), "task", "update") {
				public void action() {
					SayingCallSigns();
				}
			});
		} else {
			trialNumber++;
			
			addUpdate(0.50);
		}

	};

	public void error(String s) {
		System.out.println("ERROR: " + s);
	}

	public Result analyze(Task[] tasks, boolean output) {
		// double[] modelTimes = {calculateAverage(responses)};

		ATC_old task = (ATC_old) tasks[0];
		
		for (int i = 0; i < NUMBER_OF_TRIALS; i++) {
			int index =i*NUMBER_OF_PLANES;
			double summationAccuracyInEachTrail = 0 ;
			for (int j = index; j < index+NUMBER_OF_PLANES; j++) {
				summationAccuracyInEachTrail += responsesLocationError[j];
			}
			meanAccuracyAtEachTrail[i]= summationAccuracyInEachTrail/NUMBER_OF_PLANES;
		}

		if (output) {
			getModel().output("\n=========  Results  ===========\n");
			getModel().output(
					"Mean Location Error ==>" + String.format("%.2f", calculateAverage(responsesLocationError)));
			getModel().output("\n");
			getModel().output(
					"Approx NO. recalled Objects ==>" + String.format("%.2f", (double)recalledObjects / NUMBER_OF_TRIALS));
			
			
			getModel().output(
					"Mean Accuracy (pixel) for every Trail :");
			getModel().output(toString(meanAccuracyAtEachTrail ,0));

		}

		// return new Result ("ATC", modelTimes, humanCounts);
		return new Result();
	}

	private double calculateAverage(List<Double> marks) {
		double sum = 0;
		if (!marks.isEmpty()) {
			for (double mark : marks) {
				sum += mark;
			}
			return sum / marks.size();
		}
		return sum;
	}

	private double calculateAverage(double[] marks) {
		double sum = 0;
		if (marks.length != 0) {
			for (double mark : marks) {
				sum += mark;
			}
			return sum / marks.length;
		}
		return sum;
	}
	
	private static String toString(double a[], int fractional) {
		String s = "";
		for (int i=0 ; i<a.length ; i++)
			s += String.format("%."+ fractional +"f",a[i]) + (i<a.length-1 ? " " : "");
		return s;
	}
}
