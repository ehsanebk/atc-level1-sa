package atc.task;

import java.awt.Color;
import java.awt.Point;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import actr.model.Chunk;
import actr.model.Event;
import actr.model.Symbol;
import actr.task.Result;
import actr.task.Task;
import actr.task.TaskCross;

public class ATC extends Task {
	private final int DISPLAY_WIDTH_2 = (1024 / 2);
	private final int DISPLAY_HEIGHT_2 = (768 / 2);
	private final int NUMBER_OF_PLANES = 12;
	private final int NUMBER_OF_TRIALS = 50;

	private int trial = 0;
	private List<Plane> planes = new Vector<Plane>();
	private int askedPlane = 0;
	private int[] responses = new int[NUMBER_OF_TRIALS];
	private double[] errors = new double[NUMBER_OF_TRIALS];
	private Values locErrors = new Values();

	public ATC() {
		super();
		setBackground(Color.GRAY);
	}

	@Override
	public void start() {
		for (int i = 0; i < NUMBER_OF_PLANES; i++) {
			String callSign = "AA" + (470 + i);
			int x = (i % 2) * DISPLAY_WIDTH_2 + random.nextInt(DISPLAY_WIDTH_2);
			int y = (i % 4 >= 2 ? 1 : 0) * DISPLAY_HEIGHT_2 + random.nextInt(DISPLAY_HEIGHT_2);
			Plane plane = new Plane(callSign, x, y);
			planes.add(plane);
		}
		addEvent(new Event(0, "task", "update") {
			@Override
			public void action() {
				showPlanes();
			}
		});
	}

	private Random random = new Random();

	private void showPlanes() {
		removeAll();
		for (Plane plane : planes)
			add(plane);
		processDisplay();
		repaint();

		double timeDelta = 0.7 * planes.size();
		addEvent(new Event(getModel().getTime() + timeDelta, "task", "update") {
			@Override
			public void action() {
				removeAll();
				add(new TaskCross(DISPLAY_WIDTH_2, DISPLAY_HEIGHT_2, 3));
				processDisplay();
				repaint();
				askPlane(random.nextInt(NUMBER_OF_PLANES));
			}
		});
	}

	private void askPlane(int index) {
		askedPlane = index;
		addAural(0.0, "call-sign", "sound", planes.get(askedPlane).getValue());
	}

	private Plane getNearestPlane(double x, double y) {
		Plane nearest = null;
		double nearestDistance = 0;
		for (Plane plane : planes) {
			Point fp = plane.getLocation();
			double fd = fp.distance(x, y);
			if (nearest == null || fd < nearestDistance) {
				nearest = plane;
				nearestDistance = fd;
			}
		}
		return nearest;
	}
	
	@Override
	public double bind(Iterator<String> it) {
		try {
			it.next();
			String cmd = it.next();
			if (cmd.equals("noise")) {
				String name = it.next();
				Chunk plane = getModel().getDeclarative().get(Symbol.get(name));
				double activation = plane.getBaseLevel(); // getActivation();
				double threshold = 0.5;

				double base = .01;
				double scale = .1;
				double noise = base + scale * (activation >= threshold ? Math.exp(-activation + threshold) : 0);

				// double base = .0;
				// double scale = .1;
				// double noise = Math.min( Math.pow(NUMBER_OF_PLANES/2.0, 2) /
				// 100.0 , 0.20 );

//				 System.out.println("noise = " + noise);
//				 System.out.println("activation = " + activation);

				return noise;
			} else
				return 0;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public void clickMouse() {
		double mouseX = getMouseX() - 20; // there is a 20 pixels error for the X coordinates in the architecture
		double mouseY = getMouseY() - 10; // there is a 10 pixels error for the Y coordinates in the architecture
		Plane nearest = getNearestPlane(mouseX, mouseY);
		boolean correct = (nearest == planes.get(askedPlane));
		responses[trial] = correct ? 1 : 0;
		if (correct)
			locErrors.add(planes.get(askedPlane).getLocation().distance(mouseX, mouseY));
		
		errors[trial] = planes.get(askedPlane).getLocation().distance(mouseX, mouseY); // there is a error of 22.4
		trial++;
		if (trial < NUMBER_OF_TRIALS)
			showPlanes();
		else
			getModel().stop();
	}

	@Override
	public Result analyze(Task[] tasks, boolean output) {
		try {
			Values allResponses = new Values();
			Values allErrors = new Values();
			Values allLocErrors = new Values();
			Values[] responseValues = new Values[NUMBER_OF_TRIALS];
			Values[] errorValues = new Values[NUMBER_OF_TRIALS];
			for (int i = 0; i < NUMBER_OF_TRIALS; i++) {
				responseValues[i] = new Values();
				errorValues[i] = new Values();
			}
			for (Task taskCast : tasks) {
				ATC task = (ATC) taskCast;
				for (int i = 0; i < NUMBER_OF_TRIALS; i++) {
					allResponses.add(task.responses[i]);
					responseValues[i].add(task.responses[i]);

					allErrors.add(task.errors[i]);
					errorValues[i].add(task.errors[i]);

				}
				allLocErrors.add(task.locErrors.mean());
			}

			DecimalFormat df1 = new DecimalFormat("#.0");
			DecimalFormat df3 = new DecimalFormat("#.000");

			getModel().output("\n=========  Results  ===========\n");

			getModel().output("Overall Correctness: " + df3.format(allResponses.mean()));
			getModel().output("Overall Error: " + df1.format(allErrors.mean()));
			getModel().output("Overall Location Error: " + df1.format(allLocErrors.mean()));

			getModel().output("\nCorrectness by Trial:");
			String s = "";
			for (int i = 0; i < NUMBER_OF_TRIALS; i++)
				s += df3.format(responseValues[i].mean()) + "\t";
			getModel().output(s.trim());

			getModel().output("\nLocation Error by Trial:");
			s = "";
			for (int i = 0; i < NUMBER_OF_TRIALS; i++)
				s += df1.format(errorValues[i].mean()) + "\t";
			getModel().output(s.trim());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new Result();
	}
}
